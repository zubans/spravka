require 'test_helper'

class SessionsControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get sessions_new_url
    assert_response :success
  end

  test "should get create" do
    get sessions_create_url
    assert_response :success
  end

  test "should get destroy" do
    get sessions_destroy_url
    assert_response :success
  end

  test "should get view" do
    get sessions_view_url
    assert_response :success
  end

  test "should get session_params" do
    get sessions_session_params_url
    assert_response :success
  end

end
