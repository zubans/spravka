require 'test_helper'

class SpravkaLivesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @spravka_life = spravka_lives(:one)
  end

  test "should get index" do
    get spravka_lives_url
    assert_response :success
  end

  test "should get new" do
    get new_spravka_life_url
    assert_response :success
  end

  test "should create spravka_life" do
    assert_difference('SpravkaLife.count') do
      post spravka_lives_url, params: { spravka_life: { link_pattern: @spravka_life.link_pattern, name_s: @spravka_life.name_s, type_s: @spravka_life.type_s } }
    end

    assert_redirected_to spravka_life_url(SpravkaLife.last)
  end

  test "should show spravka_life" do
    get spravka_life_url(@spravka_life)
    assert_response :success
  end

  test "should get edit" do
    get edit_spravka_life_url(@spravka_life)
    assert_response :success
  end

  test "should update spravka_life" do
    patch spravka_life_url(@spravka_life), params: { spravka_life: { link_pattern: @spravka_life.link_pattern, name_s: @spravka_life.name_s, type_s: @spravka_life.type_s } }
    assert_redirected_to spravka_life_url(@spravka_life)
  end

  test "should destroy spravka_life" do
    assert_difference('SpravkaLife.count', -1) do
      delete spravka_life_url(@spravka_life)
    end

    assert_redirected_to spravka_lives_url
  end
end
