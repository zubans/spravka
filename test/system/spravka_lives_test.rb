require "application_system_test_case"

class SpravkaLivesTest < ApplicationSystemTestCase
  setup do
    @spravka_life = spravka_lives(:one)
  end

  test "visiting the index" do
    visit spravka_lives_url
    assert_selector "h1", text: "Spravka Lives"
  end

  test "creating a Spravka life" do
    visit spravka_lives_url
    click_on "New Spravka Life"

    fill_in "Link Pattern", with: @spravka_life.link_pattern
    fill_in "Name S", with: @spravka_life.name_s
    fill_in "Type S", with: @spravka_life.type_s
    click_on "Create Spravka life"

    assert_text "Spravka life was successfully created"
    click_on "Back"
  end

  test "updating a Spravka life" do
    visit spravka_lives_url
    click_on "Edit", match: :first

    fill_in "Link Pattern", with: @spravka_life.link_pattern
    fill_in "Name S", with: @spravka_life.name_s
    fill_in "Type S", with: @spravka_life.type_s
    click_on "Update Spravka life"

    assert_text "Spravka life was successfully updated"
    click_on "Back"
  end

  test "destroying a Spravka life" do
    visit spravka_lives_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Spravka life was successfully destroyed"
  end
end
