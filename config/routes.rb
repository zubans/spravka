Rails.application.routes.draw do
  root "spravka_lives#index"
  
  get 'login', to: 'sessions#new'
  get 'logout', to: 'sessions#destroy'
  
  get 'spravka_lives/close/:id' , to: 'spravka_lives#close', as: 'close'
  patch 'spravka_lives/change_flag/:id', to: 'spravka_lives#change_flag', as: 'change_flag'
  get 'archive', to: 'otdels#archive'
  patch 'otdels.:id', to: 'otdels#delete_position'
  get 'otdels/new_position/:id', to: 'otdels#new_position', as: 'new_position'
  post 'new_position/:otdel_id', to: 'otdels#create_position'
  resources :otdels
  resources :sessions, only: [:create]
  resources :users, only: [:new, :create]
  resources :spravka_lives
  
  
  
  
 end
