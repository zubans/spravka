# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_17_124040) do

  create_table "comonforms", force: :cascade do |t|
    t.string "fields"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fields_for_spravkas", force: :cascade do |t|
    t.string "fields"
    t.integer "stype_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["stype_id"], name: "index_fields_for_spravkas_on_stype_id"
  end

  create_table "otdel_positions", force: :cascade do |t|
    t.integer "otdel_id"
    t.integer "position_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["otdel_id"], name: "index_otdel_positions_on_otdel_id"
    t.index ["position_id"], name: "index_otdel_positions_on_position_id"
  end

  create_table "otdels", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "positions", force: :cascade do |t|
    t.string "name"
    t.integer "otdel_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["otdel_id"], name: "index_positions_on_otdel_id"
  end

  create_table "spravka_lives", force: :cascade do |t|
    t.string "name_s"
    t.string "department"
    t.string "position"
    t.string "place"
    t.integer "stype_id"
    t.string "stavka"
    t.date "prikaz_date"
    t.string "prikaz_n"
    t.date "date_start"
    t.date "date_finish"
    t.integer "flag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name_s"], name: "index_spravka_lives_on_name_s"
  end

  create_table "stypes", force: :cascade do |t|
    t.string "name_s"
    t.boolean "spravka_exist"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "nick"
    t.string "password_digest"
    t.integer "privilegies"
    t.datetime "last_login"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
