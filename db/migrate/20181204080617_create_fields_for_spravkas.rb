class CreateFieldsForSpravkas < ActiveRecord::Migration[5.2]
  def change
    create_table :fields_for_spravkas do |t|
      t.string :fields
      t.references :stype, index: true

      t.timestamps
    end
  end
end
