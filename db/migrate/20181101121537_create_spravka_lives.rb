class CreateSpravkaLives < ActiveRecord::Migration[5.2]
  def change
    create_table :spravka_lives do |t|
      t.string :name_s
      
      t.string  :department
      t.string  :position
      t.string  :place
      t.integer :stype_id
      t.string  :stavka
      t.date    :prikaz_date
      t.string  :prikaz_n
      t.date    :date_start
      t.date    :date_finish
      
      
      t.integer :flag
      
      t.timestamps
    end
    add_index :spravka_lives, :name_s
    #Ex:- add_index("admin_users", "username")
  end
end
