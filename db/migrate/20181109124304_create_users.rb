class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nick
      t.string :password_digest
      t.integer :privilegies
      t.datetime :last_login

      t.timestamps
    end
  end
end
