class CreateOtdels < ActiveRecord::Migration[5.2]
  def change
    create_table :otdels do |t|
      t.string :name

      t.timestamps
    end
  end
end
