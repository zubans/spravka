class CreateOtdelPositions < ActiveRecord::Migration[5.2]
  def change
    create_table :otdel_positions do |t|
      t.references :otdel
      t.references :position

      t.timestamps
    end
  end
end
