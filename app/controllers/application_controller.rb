class ApplicationController < ActionController::Base
    before_action :authenticated_user!, except: [:new, :create]
    def current_user
        if !session[:user_id].blank?
            @user ||= User.find(session[:user_id])
        end
    end
    
    def authenticated_user!
        if current_user.nil?
            flash[:error] = "Войдите в учетную запись отдела кадров"
            redirect_to login_path
        end
    end

end
