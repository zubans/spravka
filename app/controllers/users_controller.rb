class UsersController < ApplicationController
  skip_before_action :authenticated_user!
  before_action :reset_session
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.last_login = Time.now

    if @user.save
      session[:user_id] = @user.id
      flash[:success] = 'Ваш аккаунт создан, можно приступить к работе'
      redirect_to root_path
    else
      render :new
  end
end


def user_params
  params.require(:user).permit(:nick, :password, :password_confirmation, :last_login)
end

end
