class SessionsController < ApplicationController
  skip_before_action :authenticated_user!, except: [:destroy]
  def new
  end

  def create
    reset_session
    @user = User.find_by(nick: session_params[:nick])

    if @user && @user.authenticate(session_params[:password])
      session[:user_id] = @user.id
      session[:nick] = @user.nick
      
      @user.update(last_login: Time.now)
      

      flash[:success] = "Добро пожаловать #{@user.nick}"
      redirect_to root_path

    else
      flash[:error] = "Неверное имя пользователя или пароль"
      redirect_to login_path
    end

  end

  def destroy
    reset_session
  end

  def view
    redirect_to login_path
  end

  def session_params
    params.require(:session).permit(:nick, :password, :last_login)
  end
end
