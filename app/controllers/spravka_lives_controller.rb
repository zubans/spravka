class SpravkaLivesController < ApplicationController
  #skip_before_action :authenticate_user!, :except => :destroy 
  before_action :set_spravka_life, only: [:show, :edit, :update, :destroy, :change_flag]
  
  def titleize(string)
    string.split(" ").map {|word| word.capitalize}.join(" ")
  end

  def gen(gena)
    if(gena == 'male')
      puts "он"
    else
      puts "она"
    end
  end

  # GET /spravka_lives
  # GET /spravka_lives.json
  
  def index
    @spravka_lives = SpravkaLife.where(flag: [0,1])
    #@spravka_lives_inprogress = SpravkaLife.where(flag: 1)
    @type_s = Stype.all
    
    
  end

  # GET /spravka_lives/1
  # GET /spravka_lives/1.json
  def show
    @print_form = true
    @spravka_lives = SpravkaLife.find(params[:id])
    name_for_datting = @spravka_life.name_s.split
    gender = Petrovich(firstname: name_for_datting[1]).gender
    @spravka_life.name_s = Petrovich(lastname: name_for_datting[0],firstname: name_for_datting[1], middlename: name_for_datting[2], gender: gender).genitive.to_s
        
    #render file: "/public/trudbook.xml", object: @spravka_lives

  end

  def close
    @print_form = true
    @spravka_lives = SpravkaLife.find(params[:id])
    name_for_datting = @spravka_lives.name_s.split
    @spravka_lives.name_s = Petrovich(lastname: name_for_datting[0],firstname: name_for_datting[1], middlename: name_for_datting[2]).dative.to_s
    gender = Petrovich(firstname: name_for_datting[1]).gender
    @gender = gen(gender)


        
  end

  def change_flag
    @spravka_lives = SpravkaLife.find(params[:id])
    @spravka_lives.update_attribute(:flag, "1")
    redirect_to root_path, notice: 'Данные справки обновлены' 

  end
  
  # GET /spravka_lives/new
  def new
    @spravka_life = SpravkaLife.new
    @type_s = Stype.all
    @comonform= Comonform.find_by(id: 1)
  end

  # GET /spravka_lives/1/edit
  def edit
    @type_s = Stype.all
    @flag = 1
    active_spravka = SpravkaLife.find(params[:id])
    
    @fields = FieldsForSpravka.find_by(stype_id: active_spravka.stype_id)
    @comonform= Comonform.find_by(id: 1)
    
    
    #@fields_work_book = []
    #@fields_work_place = {stavka:["Количество ставок",:text_field],prikaz_date:["Дата приказа", :date_field],prikaz_n:["Номер приказа", :text_field]}
    #@fields_vacation = {start_date:["начало отпуска",:date_field],finish_date:["Конец отпуска", :date_field],prikaz_date:["Дата приказа",:date_field], prikaz_n:["Номер приказа", :text_field]}
  end
  


  # POST /spravka_lives
  # POST /spravka_lives.json
  def create
    @comonform= Comonform.find_by(id: 1)
    @spravka_life = SpravkaLife.new(spravka_life_params)
    respond_to do |format|
      @spravka_life.flag = 0
      @spravka_life.position = @spravka_life.position.downcase
      @spravka_life.name_s = titleize(@spravka_life.name_s)  
      
      if @spravka_life.save
        
        format.html { redirect_to new_spravka_life_url, notice: 'Справка успешно создана' }
        format.json { render :show, status: :created, location: @spravka_life }
      else
        format.html { render :new }
        format.json { render json: @spravka_life.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /spravka_lives/1
  # PATCH/PUT /spravka_lives/1.json
  def update
    respond_to do |format|
      if @spravka_life.update(spravka_life_params)
        format.html { redirect_to @spravka_life, notice: 'Данные справки обновлены' }
        format.json { render :show, status: :ok, location: @spravka_life }
      else
        format.html { render :edit }
        format.json { render json: @spravka_life.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /spravka_lives/1
  # DELETE /spravka_lives/1.json
  def destroy
    @spravka_life.destroy
    respond_to do |format|
      format.html { redirect_to spravka_lives_url, notice: 'Справка удалена' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_spravka_life
      @spravka_life = SpravkaLife.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def spravka_life_params
      params.require(:spravka_life).permit(:id, :name_s, :stype_id, :department, :position, :place, :stavka, :prikaz_date, :prikaz_n, :flag, stype_attributes: [:id, :name_s])
    end
end
