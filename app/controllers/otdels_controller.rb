class OtdelsController < ApplicationController
  
  def index
    @otdels = Otdel.all
    @otdel_position = OtdelPosition.all
    @positions = Position.all
  end

  def new
    @otdel = Otdel.new
  end

  def new_position
    @otdel = Otdel.find(params[:id])
    @position = Position.new
    #@otdel_position = OtdelPosition.new
  end

  def create_position
    @position = Position.create(position_params)
    @position = Position.last
    @otdel_position = OtdelPosition.create(otdel_id: params[:otdel_id], position_id: @position.id)
    redirect_to otdels_path
  end

  def create
    @otdel = Otdel.new(otdel_params)
    if @otdel.save!
      redirect_to otdels_path
    end
  end

  def destroy
    @otdel = Otdel.find(params[:id])
    @otdel.destroy
    redirect_to otdels_path
  end

  def delete_position    
    @otdel_position = OtdelPosition.find(params[:id])
    @position = Position.find_by(id: @otdel_position.position_id)
    @position.destroy
    @otdel_position.destroy
    redirect_to otdels_path
  end

    private
    def otdel_params
      params.require(:otdel).permit(:name, positions_attributes: [:id, :name])
    end

    def position_params
      params.require(:position).permit(:id,:name)
    end
end
