class User < ApplicationRecord
  has_secure_password
  include ActiveModel::SecurePassword
  validates :nick, uniqueness: true, presence: true
end
