class SpravkaLife < ApplicationRecord
  has_one :stype  
  validates_format_of :name_s, :with => /\A(?=.* )[^0-9`!@#\\\$%\^&*\;+_=]{4,}\z/, :message => "ФИО должно быть полное(Фамилия Имя Отчество)"
  validates_presence_of :name_s, :department, :position, :place, presence: true, message: "Поле не должно быть пустым"
end
