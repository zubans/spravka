class Otdel < ApplicationRecord
  has_many :positions, through: :otdel_positions
  accepts_nested_attributes_for :positions
end
