class FieldsForSpravka < ApplicationRecord
  has_many :stype
  belongs_to :stype
  serialize :fields, Hash
  accepts_nested_attributes_for :stype
end