json.extract! spravka_life, :id, :name_s, :type_s, :link_pattern, :created_at, :updated_at
json.url spravka_life_url(spravka_life, format: :json)
